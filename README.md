"# My project's README" 

Class Fraction describes an object, Fraction, with integer denominator and numerator values.
*Fractions can be multiplied by, divided by, added to and subtracted from other fractions.
*Fractions can be cast to type double.
*Fractions are simplified on construction using the euclidean algorithm.