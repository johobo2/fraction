#ifndef FRACTION_H_INCLUDED
#define FRACTION_H_INCLUDED
#include <iostream>

int gcd(int a, int b);

class Fraction
{
private:
    int denominator;
    int numerator;

    void simplify();

public:
    Fraction(int denominator, int numerator);
    Fraction(const Fraction& copy) = default;
    Fraction(Fraction&& move) = default;
    ~Fraction() = default;

    Fraction& operator=(const Fraction& copy) = default;
    Fraction& operator=(Fraction&& move) = default;

    Fraction operator*(const Fraction&) const;
    Fraction operator/(const Fraction&) const;
    Fraction operator+(const Fraction&) const;
    Fraction operator-(const Fraction&) const;

    operator double();
    friend std::ostream& operator<<(std::ostream& os, const Fraction& fraction);
};

std::ostream& operator<<(std::ostream& os, const Fraction& fraction);


#endif // FRACTION_H_INCLUDED
