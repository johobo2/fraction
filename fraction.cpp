#include "fraction.h"

int gcd(int a, int b)
{
    if(a ==0) return b;
    if(b ==0) return a;
    return gcd(b, a%b);
}

std::ostream& operator<<(std::ostream& os, const Fraction& fraction)
{
    os << '(' << fraction.denominator << '/' << fraction.numerator << ')';
    return os;
}

void Fraction::simplify()
{
    int divisor = gcd(this->denominator, this->numerator);

    this->denominator /= divisor;
    this->numerator /= divisor;
}

Fraction::Fraction(int denominator, int numerator = 1):
    denominator(denominator),
    numerator(numerator)
{
    simplify();
}

Fraction Fraction::operator*(const Fraction& other) const
{
    int result_denominator = this->denominator * other.denominator;
    int result_numerator = this->numerator * other.numerator;
    return Fraction(result_denominator, result_numerator);
}

Fraction Fraction::operator/(const Fraction& other) const
{
    int result_denominator = this->denominator * other.numerator;
    int result_numerator = this->numerator * other.denominator;
    return Fraction(result_denominator, result_numerator);
}

Fraction Fraction::operator+(const Fraction& other) const
{
    int result_denominator = (this->denominator * other.numerator) + (this->numerator * other.denominator);
    int result_numerator = this->numerator * other.numerator;
    return Fraction(result_denominator, result_numerator);
}

Fraction Fraction::operator-(const Fraction& other) const
{
    int result_denominator = (this->denominator * other.numerator) - (this->numerator * other.denominator);
    int result_numerator = this->numerator * other.numerator;
    return Fraction(result_denominator, result_numerator);
}

Fraction::operator double()
{
    return (double)denominator/numerator;
}
